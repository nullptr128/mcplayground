export function float32ArrayToBinaryBuffer(float32Array: Float32Array) {
    // Calculate the size of the buffer needed to hold the bits
    // Each element in the float32Array represents a bit, and we pack 32 bits into each 32-bit integer
    const bufferSize = Math.ceil(float32Array.length / 32);
    const buffer = new ArrayBuffer(bufferSize * 4); // 4 bytes per 32-bit integer
    const view = new Uint32Array(buffer);

    float32Array.forEach((value, index) => {
        const bufferIndex = Math.floor(index / 32); // Find which 32-bit integer this bit belongs to
        const bitPosition = index % 32; // Find the position of the bit within the 32-bit integer
        if (value === 1) {
            // Set the bit at the bitPosition to 1
            view[bufferIndex] |= (1 << bitPosition);
        }
        // No need to explicitly set bits to 0, ArrayBuffer initializes with 0s
    });

    return Buffer.from(buffer);
}
