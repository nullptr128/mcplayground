import _ from 'lodash';

async function _main() {

    const mode = process.argv[2];
    let TF;

    switch (mode) {
        case 'js':
            TF = require('@tensorflow/tfjs');
            break;
        case 'gpu':
            TF = require('@tensorflow/tfjs-node-gpu');
            break;
        case 'cpu':
            TF = require('@tensorflow/tfjs-node');
            break;
        default:
            throw new Error('Invalid mode');
    }
        
    const PRODUCT_COUNT = 800;
    const FILTERS_COUNT = 191;
    const ATTR_COUNT = 5000;

    // generate filter for 191 filters with vectors of 5000 elements
    const filterVector = Array.from({ length: FILTERS_COUNT }, () => Array.from({ length: ATTR_COUNT }, () => 0));

    // select two random filters on every filter vector
    filterVector.forEach((filter) => {
        const randomIndex1 = Math.floor(Math.random() * ATTR_COUNT);
        const randomIndex2 = Math.floor(Math.random() * ATTR_COUNT);
        filter[randomIndex1] = 1;
        filter[randomIndex2] = 1;
    });

    const filterSums = filterVector.map((filter) => _.sum(filter));

    // generate product vector of 800 products with 12000 elements each
    const productsData = Array.from({ length: PRODUCT_COUNT }, () => Array.from({ length: ATTR_COUNT }, () => Math.floor(Math.random() * 2)));

    // console.log('Initial data:', { filterVector, productsData, filterSums });

    const filtersTensor = TF.tensor2d(filterVector);
    const productsTensor = TF.tensor2d(productsData);

    const _start = performance.now();
        const results = TF.matMul(filtersTensor, productsTensor.transpose()).arraySync() as number[][];
    const _end = performance.now();
        
    // console.log('Results:', results);

    const counts: number[] = results.map((result, index) => _.sumBy(result, (value) => value === filterSums[index] ? 1 : 0));
    console.log({ 
        totalFilters: FILTERS_COUNT, 
        totalAttributes: ATTR_COUNT, 
        totalProducts: PRODUCT_COUNT, 
        counts: counts, /*countsSum: _.sum(counts), */ 
        time: (_end - _start) 
    });

}

_main();
